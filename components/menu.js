import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';

const Menu = () => {
  return(
	<View style={styles.container}>
		<ImageBackground source={require("img/ico-menu.png")} style={styles.menu}></ImageBackground>
		<Text style={styles.text}>Курсы валют в Петропавловске на сегодня</Text>
	</View>
  )
}

const styles = StyleSheet.create({
  menu: {
    height: 40,
    width: 40,
  },
  text: {
    color: '#fff',
    alignSelf: 'flex-end',
    marginLeft: 15,
    width: '80%'
  },
  container: {
    padding: 15,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  }
});

export default Menu;
 
