import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

//components
import BankItem from '../components/bankItem';

const BankList = (props) => {

	const items = props.banks.map((item) => {
		return(
			<View>
        <BankItem bankitem={item} />
      </View>
		)
	});
	
  return (
    <View style={styles.white}>
      {items}
    </View>
  )
}

const styles = StyleSheet.create({
  white:{
    color: '#fff',
    padding: 15 
  },
})

export default BankList;
