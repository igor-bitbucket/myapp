import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const BankItem = (props) => {

  return(
    <View style={styles.item}>

      <View style={styles.text}>
        <Text style={styles.white}>{props.bankitem.title}</Text>
        <Text>Обновлено:</Text>
      </View>

      <View>
        <Image source={require('../img/rub.png')} />
        <View>{props.bankitem.buy_rub}</View>
        <View>{props.bankitem.sale_rub}</View>
      </View>

      <View>
        <Image source={require('../img/usd.png')} />
        <View>{props.bankitem.buy_usd}</View>
        <View>{props.bankitem.sale_usd}</View>
      </View>

      <View>
        <Image source={require('../img/eur.png')} />
        <View>{props.bankitem.buy_eur}</View>
        <View>{props.bankitem.sale_eur}</View>
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  white: {
    color: '#fff',
    opacity: 1
  },
  item: {
    backgroundColor: 'rgba(13,236,247,0.6)',
    marginBottom: 10,
    borderRadius: 3,
    padding: 15,
    flexDirection: 'row'
  },
})

export default BankItem;