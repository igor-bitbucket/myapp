import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';

import JSON from './banks.json';

//components
import Menu from './components/menu';
import BankList from './components/bankList';

export default class App extends React.Component{
	
	state = {
		banks: JSON,
	}
	
	render() {
		return (
		  <ImageBackground source={require("img/bg.jpg")} style={{width: '100%', height: '100%'}} imageStyle={{resizeMode: 'stretch'}}>
			<Menu />
			<View style={styles.container}>
			  <BankList banks={this.state.banks} />
			</View>
		  </ImageBackground>
		);
	}
}

const styles = StyleSheet.create({
  container: {
  },
  main: {
    color: '#fff',
  },
});

